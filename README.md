### Overview
This package takes a transition matrix R for an orbit control problem as input, then does the following:

1. Generates pre-training data consisting of randomly selected states and corresponding actions chosen using the pseudoinverse of R;

2. Pre-trains a policy and Q function based on the data from step 1 for use as a "warm start" in step 3;

3. Starts with the policy and Q function from 2 and subsequently uses the Deep Deterministic Policy Gradient reinforcement learning algorithm to train an alternative policy for the orbit control problem by interacting with a simulated version of the problem.

### Basic Usage
First save your transition matrix R in `.npy` file format, say `mymatrix.npy`, in the **data** directory. The **data** directory currently contains several different matrices to try: `R_Matrix.npy` is the full 360 by 360 matrix, while `R12_Matrix.npy` is 12 by 12, `R24_Matrix.npy` is 24 by 24, etc.

Next, open `config.yml` in the **scripts** directory, and set `matrix_filename: '../data/mymatrix.npy'`. `config.yml` is where you can make changes to the various settings for the `generate_data.py, pretrain.py`, and `train.py` files, as well as the hyperparameters of the algorithms involved. The settings in `config.yml` that are currently included in the package are suitable for a relatively small R matrix, say 24 by 24. As R becomes larger, the amount of training data needed, the sizes of the neural networks, and the length of the training sessions must scale up, too.

Once you have the configurations you want, from the **scripts** directory you can simply:

1. Generate pre-training data by doing `python generate_data.py`;

2. Pre-train by doing `python pretrain.py`;

3. Train by doing `python train.py`.

### Checkpointing and Restarting
At the end of every pre-training and training session, a copy of the parameters is logged in the **data** directory. You can resume training from where you left off by setting `pretrain_loading_checkpoint: True` or `train_load_checkpoint: True` in `config.yml`.

### Training Results
Copies of the training and validation errors are saved to appropriately named `.npy` files in the **data** directory at the end of each training session.
