# Environments

import numpy as np

import rlcontrol.core.utils as utils


class Env:
    """General environment class for a linear system with noise.
    
    If only the dynamics matrix and noise model are provided, the default
    is goal state equal to the origin, reward function the square distance from
    the goal state, and initial state equal to the goal state.
    
    User can optionally pass either a fixed vector or a function that
    depends on the current timestep as the goal.
    
    User can specify a reward that is a function of the current state,
    action, and timestep.
    """
    
    def __init__(self, dynamics_matrix_filename, noise_model,
                 goal=None, reward_func=None, init_state=None,
                 state_clip_radius=None):
        self.R = np.load(dynamics_matrix_filename)
        self.state_dim = self.R.shape[0]
        self.action_dim = self.R.shape[1]
        self.noise = noise_model # use noise models from utils
        
        # goal state is the origin by default, otherwise it stores the
        # goal state
        self.goal = goal if goal is not None else \
            np.zeros(self.state_dim)
            
        # reward of a state is squared distance from goal state by default
        self.reward = reward_func if reward_func is not None else \
            lambda x: np.square(x - self.goal).sum()
            
        # initialize to goal state
        self.state = init_state if init_state is not None else \
            self.goal

        # constrain state to remain within certain distance of the origin
        # TODO: constrain to lie close to goal state, instead
        self.state_clip_radius = state_clip_radius

    def _clip(self, state):
        radius = self.state_clip_radius
        return np.clip(state, -radius, radius) if radius is not None \
                else state
        
    def _next_state(self, state, action):
        return self._clip(
            state - self.R.dot(action) + self.noise.make_noise(state))

    def transition(self, action):
        """Update the timestep, compute the reward, update the state, and
        return the reward and the new direction that we need to go in to get
        back to the goal state.
        """

        self.state = self._next_state(self.state, action)
        reward = self.reward(self.state)
        return reward, self.state - self.goal

    def hypothetical_reward(self, state, action):
        """Take a given state and action and compute what the reward
        would be for the new state.
        """

        return self.reward(self._next_state(state, action))
    

class ProjectEnv(Env):
    """Environment with user-specified noise model and goal vector."""
    
    def __init__(self, dynamics_matrix_filename,
                 noise_model, goal_vector=None, state_clip_radius=None):
        
        Env.__init__(self, dynamics_matrix_filename, noise_model,
                     goal=goal_vector,
                     state_clip_radius=state_clip_radius)
        
    def _reward(self):
        """Get reward based on current state."""
        return self.reward(self.state)
        
        
class GaussianProjectEnv(ProjectEnv):
    """Project environment with Gaussian noise and origin as goal state.
    
    TODO: allow custom covariance and mean.
    """
    
    def __init__(self, dynamics_matrix_filename,
                 mean=None, cov=None, state_clip_radius=None):
        
        ProjectEnv.__init__(self, dynamics_matrix_filename,
                            None, state_clip_radius=state_clip_radius)
        
        self.noise = utils.GaussianNoise(self.state_dim, mean, cov)


class NoiselessProjectEnv(ProjectEnv):
    """Project environment with no noise and origin as goal state."""

    def __init__(self, dynamics_matrix_filename, state_clip_radius=None):

        ProjectEnv.__init__(self, dynamics_matrix_filename,
                            None, state_clip_radius=state_clip_radius)

        self.noise = utils.NoNoise(self.state_dim)
    
    
    
    
    
    
    
    
# env
