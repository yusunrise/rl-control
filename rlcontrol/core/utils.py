"""Provide some utilities, such as a replay buffer for RL agents,
noise models for the environment and exploration, and tools for pretraining
data generation.
"""

import numpy as np
import torch
import random
from collections import deque
import yaml
import copy


# utility functions
def load_config(filename):
    with open(filename, 'r') as f:
        config = yaml.safe_load(f)
    return config

def convert_to_tensors(device, arrays):
    """Convert multiple numpy arrays to tensors."""
    
    return tuple([torch.FloatTensor(array).to(device) for array in arrays])
    
def copy_parameters(model1, model2):
    """Overwrite model1's parameters with model2's."""
    
    model1.load_state_dict(model2.state_dict())

def inference_function(policy_network, models_filename):
    """
    Take a policy network with architecture identical to the one to
    be loaded and the filename of the state_dict containing its parameters,
    then return a function that uses that policy to return an action when
    given a state.
    """

    policy = copy.deepcopy(policy_network)
    models = torch.load(models_filename)
    policy.load_state_dict(models['pi_state_dict'])
    policy.eval()

    def no_grad_policy(state):
        state = torch.FloatTensor(state)
        return policy(state).cpu().detach().numpy()

    return no_grad_policy


class Buffer:
    """Circular experience replay buffer."""
    
    def __init__(self, max_length):
        self.buffer = deque(maxlen=max_length)
        
    def sample_batch(self, batch_size):
        batch = random.sample(self.buffer, batch_size)
        states, actions, rewards, next_states = [], [], [], []
        for elem in batch:
            s, a, r, s2 = elem
            states.append(s)
            actions.append(a)
            rewards.append(r)
            next_states.append(s2)
        return (states, actions, rewards, next_states)
    
    def add(self, sample):
        self.buffer.append(sample)
        
        
class DataGenerator:
    """Generator for making and saving pre-training data from user-provided
    state and action-generating functions.
    """
    
    def __init__(self, state_generator, action_generator, num_samples):
        self.state_generator = state_generator
        self.action_generator = action_generator
        self.num_samples = num_samples
        self.data = []
        
    def generate_data(self):
        states, actions = [], []
        for _ in range(self.num_samples):
            state = self.state_generator()
            action = self.action_generator(state)

            states.append(state)
            actions.append(action)

        temp = [states, actions]
        self.data = tuple([np.array(elem) for elem in temp])
            
    def save_data(self, filename):
        np.savez(filename, *self.data)


class NoiseModel:
    """General noise model prototype."""
    def __init__(self):
        pass
    
    def make_noise(self, state=None, time=None):
        pass
    

class OUNoise(NoiseModel):
    """Generates noise via an Ornstein-Uhlenbeck process."""
    
    # TODO: make it work correctly
    def __init__(self, dim, theta=1, mu=None, sigma=None, delta=1):
        
        NoiseModel.__init__(self)
        
        self.dim = dim
        self.theta = theta
        
        self.mu = mu if mu is not None else np.zeros(self.dim)
        self.sigma = sigma if sigma is not None else 1
            
        self.delta = delta
        self.state = self.mu
        
    def reset(self):
        self.state = self.mu
        
    def evolve_state(self):
        step = self.theta * (self.mu - self.state) * self.delta + \
            self.sigma * np.sqrt(self.delta) * np.random.multivariate_normal(
                    self.mu, np.eye(self.dim))
        self.state += step
        return self.state
        
    def make_noise(self, state):
        return self.evolve_state()
    
class GaussianNoise(NoiseModel):
    """Generate Gaussian noise."""
    
    def __init__(self, dim, mean=None, cov=None):
        
        NoiseModel.__init__(self)
        
        self.dim = dim
        
        if mean is not None:
            self.mean = mean
        else:
            self.mean = np.zeros(self.dim)
            
        if cov is not None:
            self.cov = cov
        else:
            self.cov = np.eye(self.dim)
        
    def make_noise(self, state=None, time=None):
        return np.random.multivariate_normal(self.mean, self.cov)
        
        
class NoNoise(NoiseModel):
    """Generate no noise."""

    def __init__(self, dim):

        NoiseModel.__init__(self)

        self.dim = dim

    def make_noise(self, state=None, time=None):
        return np.zeros(self.dim)




# end
