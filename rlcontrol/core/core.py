"""Core objects for the project.

Contains the DDPG RL agent and pretraining Trainer.
"""

import numpy as np
import torch
import torch.nn.functional as F
import torch.utils.data
import copy
import warnings

import rlcontrol.core.utils as utils


class Trainer:
    """Train a model.
    
    Takes a user-specified model, optimizer, loss criterion, training set,
    etc. Trains the model and saves the parameters to a user-specified path.
    
    This is meant to pretrain a policy to be used as a 'warm start' during
    the RL training process.
    """
    def __init__(self, pi, Q, env, training_set_path, test_set_path,
                 parameter_save_path, errors_save_path,
                 num_epochs, batch_size, gamma,
                 pi_optim=torch.optim.Adam,
                 Q_optim=torch.optim.Adam,
                 pi_lr=0.001, Q_lr=0.001,
                 pi_criterion=torch.nn.MSELoss(),
                 Q_criterion=torch.nn.MSELoss(),
                 log_interval=1000, enable_cuda=True):
    
        self.pi = pi
        self.Q = Q
        self.env = env
        self.training_set_path = training_set_path
        self.test_set_path = test_set_path
        self.parameter_save_path = parameter_save_path
        self.errors_save_path = errors_save_path
        self.num_epochs = num_epochs
        self.batch_size = batch_size
        self.gamma = gamma
        self.pi_optim = pi_optim(self.pi.parameters(), lr=pi_lr)
        self.Q_optim = Q_optim(self.Q.parameters(), lr=Q_lr)
        self.pi_criterion = pi_criterion
        self.Q_criterion = Q_criterion
        self.log_interval = log_interval
        
        self._enable_cuda = enable_cuda
        self.device = torch.device(
                'cuda' if torch.cuda.is_available() and self._enable_cuda \
                else 'cpu')
        self.pi.to(self.device)
        self.Q.to(self.device)

    def _save_errors(self, errors):
        """Takes a list of errors, all of the same length, and saves them
        to the errors output file.
        """

        len_errors = len(errors[0])
        errors = [np.array(elem).reshape(len_errors,1) for elem in errors]
        errors = np.concatenate(tuple(errors), axis=1)
        np.save(self.errors_save_path, errors)

    def enable_cuda(self, enable_cuda=True, warn=True):
        """Enable or disable cuda and update models."""
        
        if warn:
            warnings.warn("Converting models between 'cpu' and 'cuda' after "
                          "initializing optimizers can give errors when using "
                          "optimizers other than SGD or Adam!")
        
        self._enable_cuda = enable_cuda
        self.device = torch.device(
                'cuda' if torch.cuda.is_available() and self._enable_cuda \
                else 'cpu')
        self.pi.to(self.device)
        self.Q.to(self.device)

    def save_checkpoint(self, filename):
        """Save state_dicts of models and optimizers."""
        
        torch.save({
                'using_cuda': self._enable_cuda,
                'pi_state_dict': self.pi.state_dict(),
                'Q_state_dict': self.Q.state_dict(),
                'pi_optimizer_state_dict': self.pi_optim.state_dict(),
                'Q_optimizer_state_dict': self.Q_optim.state_dict(),
        }, filename)
    
    def load_checkpoint(self, filename):
        """Load state_dicts for models and optimizers, prepare to resume
        training.
        """
        
        checkpoint = torch.load(filename)
        
        self.pi.load_state_dict(checkpoint['pi_state_dict'])
        self.Q.load_state_dict(checkpoint['Q_state_dict'])
        self.pi_optim.load_state_dict(checkpoint['pi_optimizer_state_dict'])
        self.Q_optim.load_state_dict(checkpoint['Q_optimizer_state_dict'])
        
        self.pi.train()
        self.Q.train()
            
        self.enable_cuda(checkpoint['using_cuda'], warn=False)

    def _generate_rewards(self, device, states, actions):
        rewards = []
        states = states.cpu().detach().numpy()
        actions = actions.cpu().detach().numpy()
        for state, action in zip(states, actions):
            rewards.append(self.env.hypothetical_reward(state, action))
        rewards = torch.FloatTensor(rewards).to(device)
        return rewards
    
    def train(self, train_pi=True):
        # load training data
        npzfile = np.load(self.training_set_path)
        states, actions = utils.convert_to_tensors(
            self.device, [npzfile[name] for name in npzfile.files])
        pretraindata = torch.utils.data.dataset.TensorDataset(
            states, actions)
        pretrainloader = torch.utils.data.DataLoader(
                pretraindata, batch_size=self.batch_size,
                shuffle=True, num_workers=0)

        # load test data
        test_npzfile = np.load(self.test_set_path)
        states, actions = utils.convert_to_tensors(
            self.device,
            [test_npzfile[name] for name in test_npzfile.files])
        pretestdata = torch.utils.data.dataset.TensorDataset(
            states, actions)
        pretestloader = torch.utils.data.DataLoader(
                pretestdata, batch_size=self.batch_size,
                shuffle=True, num_workers=0)

        # train, test, print out progress, save results
        pi_train_errors, pi_test_errors, Q_train_errors, Q_test_errors = \
                [], [], [], []
        errors = [pi_train_errors, pi_test_errors, Q_train_errors,
                  Q_test_errors]
        for epoch in range(self.num_epochs):
            pi_running_loss = 0.0
            pi_test_running_loss = 0.0
            Q_running_loss = 0.0
            Q_test_running_loss = 0.0
            for i, sample in enumerate(pretrainloader, 0):
                states, actions = sample
                
                # fit policy to target policy
                if train_pi:
                    self.pi_optim.zero_grad()
                    outputs = self.pi(states)
                    pi_loss = self.pi_criterion(outputs, actions)
                    pi_loss.backward()
                    self.pi_optim.step()

                    pi_running_loss += pi_loss.item()
                else:
                    with torch.no_grad():
                        outputs = self.pi(states).detach()

                # fit Q function to actions and rewards selected by policy
                self.Q_optim.zero_grad()
                outputs = outputs.detach()
                rewards = self._generate_rewards(self.device, states, outputs)
                Q_input = torch.cat([states, outputs], dim=1)
                Q_outputs = self.Q(Q_input)
                Q_loss = self.Q_criterion(Q_outputs, rewards.unsqueeze(1))
                Q_loss.backward()
                self.Q_optim.step()

                Q_running_loss += Q_loss.item()
                
                if i % self.log_interval == self.log_interval-1:
                    with torch.no_grad():
                        j = 0
                        for _, sample in enumerate(pretestloader, 0):
                            j += 1
                            states, actions = sample
                            outputs = self.pi(states)

                            if train_pi:
                                pi_test_loss = self.pi_criterion(
                                    outputs, actions)
                                pi_test_running_loss += pi_test_loss.item()

                            rewards = self._generate_rewards(
                                self.device, states, outputs)
                            Q_inputs = torch.cat([states, outputs], dim=1)
                            Q_test_loss = self.Q_criterion(
                                self.Q(Q_inputs), rewards.unsqueeze(1))
                            Q_test_running_loss += Q_test_loss.item()

                        pi_running_loss = pi_running_loss / self.log_interval
                        Q_running_loss = Q_running_loss / self.log_interval
                        pi_test_running_loss = \
                                pi_test_running_loss / j
                        Q_test_running_loss = \
                                Q_test_running_loss / j

                    pi_train_errors.append(pi_running_loss)
                    pi_test_errors.append(pi_test_running_loss)
                    Q_train_errors.append(Q_running_loss)
                    Q_test_errors.append(Q_test_running_loss)

                    print('[%d, %d]: pi %.5f, test_pi %.5f, Q %.5f, test_Q %.5f' %
                          (epoch+1, i+1, pi_running_loss, pi_test_running_loss,
                           Q_running_loss, Q_test_running_loss))

                    pi_running_loss = 0.0
                    pi_test_running_loss = 0.0
                    Q_running_loss = 0.0
                    Q_test_running_loss = 0.0

                    ## save errors at each logging point
                    # self._save_errors(errors)
                   
        self._save_errors(errors)

        print("Training done.")
        
        torch.save({
            'pi_state_dict': self.pi.state_dict(),
            'Q_state_dict': self.Q.state_dict(),
        }, self.parameter_save_path)
        
        
class RLAgent:
    """General parent class for agents using specific algorithms."""
    def __init__(self, buffer, batch_size):
        pass
    
    def sample_action(self, state):
        """Generate an action based on a given state. Save and return the
        action.
        """
        pass
    
    def update(self, reward, next_state):
        """Observe the reward and the next state, then carry out an update
        step.
        """
        pass


class DDPGAgent(RLAgent):
    """Agent that carries out the DDPG algorithm."""
    
    def __init__(self, buffer, batch_size, action_noise,
                 policy_network, critic_network, policy_lr, critic_lr,
                 gamma, tau, enable_cuda=True, optimizer=torch.optim.Adam,
                 grad_clip_radius=None, action_clip_radius=None):
        
        RLAgent.__init__(self, batch_size, action_noise)
        
        # buffer and sample batch size
        self.buffer = buffer
        self.N = batch_size
        
        # networks
        self.action_noise = action_noise
        self.pi = policy_network #.to(self.device)
        self.target_pi = copy.deepcopy(policy_network)
        self.Q = critic_network #.to(self.device)
        self.target_Q = copy.deepcopy(critic_network)
        
        self._enable_cuda = enable_cuda
        self.enable_cuda(self._enable_cuda, warn=False)
        # NOTE: self.device is defined when self.enable_cuda is called!
        
        # discount factor and tau
        self.gamma = gamma
        self.tau = tau

        
        # define optimizers
        self.pi_optim = optimizer(self.pi.parameters(), lr=policy_lr)
        self.Q_optim = optimizer(self.Q.parameters(), lr=critic_lr)
        
        # keep previous state and action
        self.state = None
        self.action = None

        # clip gradients and actions?
        self.grad_clip_radius = grad_clip_radius
        self.action_clip_radius = action_clip_radius
        
        
    def sample_action(self, state):
        """Get an action based on the current state."""
        self.state = state
        state = torch.FloatTensor(state).to(self.device)
        noise = torch.FloatTensor(
                self.action_noise.make_noise(state)).to(self.device)
        action = self.pi(state) + noise
        self.action = action.cpu().detach().numpy()
        if self.action_clip_radius is not None:
            self.action = self.action.clip(-self.action_clip_radius,
                                           self.action_clip_radius)
        return self.action
        
    def _soft_parameter_update(self, params1, params2):
        for param1, param2 in zip(params1, params2):
            param1.data.copy_(
                    self.tau*param2.data + (1.0 - self.tau)*param1.data)
            
    def _convert_to_tensor(self, x):
        return torch.FloatTensor(x).to(self.device)

    def _save_rewards(self, filename):
        """Save accumulated rewards."""
        np.save(filename, self.rewards)
                               
    def update(self, reward, next_state, update_Q=True, update_pi=True):
        """Perform the DDPG update.
        
        If update_Q == update_pi == False, this step can be used to fill
        the buffer. If update_Q != update_pi == False, it can be used to
        perform policy evaluation (e.g. on a newly-loaded policy).
        """
        
        new_sample = (self.state, self.action, reward, next_state)
        self.buffer.add(new_sample)
        
        if update_Q:
            states, actions, rewards, next_states = utils.convert_to_tensors(
                    self.device, self.buffer.sample_batch(self.N))
            
            with torch.no_grad():
                target_actions = self.target_pi(next_states)
                target_Q_input = torch.cat(
                        [next_states, target_actions], dim=1)
                Q_targets = rewards.unsqueeze(1) + \
                    self.gamma * self.target_Q(target_Q_input)
                
            # minimize Q prediction error
            Q_input = torch.cat([states, actions], dim=1)
            Q_loss = F.mse_loss(self.Q(Q_input), Q_targets)
            self.Q_optim.zero_grad()
            Q_loss.backward()
            if self.grad_clip_radius is not None:
                torch.nn.utils.clip_grad_norm_(self.Q.parameters(),
                                               self.grad_clip_radius)
            self.Q_optim.step()
        
            if update_pi:
                # minimize expected cost as a function of policy
                pi_loss = self.Q(
                        torch.cat([states, self.pi(states)], dim=1)).mean()
                self.pi_optim.zero_grad()
                pi_loss.backward()
                if self.grad_clip_radius is not None:
                    torch.nn.utils.clip_grad_norm_(self.pi.parameters(),
                                                   self.grad_clip_radius)
                self.pi_optim.step()
                
            # update target parameters
            # TODO: figure out if it makes sense to update target_pi even
            # when pi is not updated
            self._soft_parameter_update(self.target_Q.parameters(),
                                        self.Q.parameters())
            self._soft_parameter_update(self.target_pi.parameters(),
                                        self.pi.parameters())


    def enable_cuda(self, enable_cuda=True, warn=True):
        """Enable or disable cuda and update models."""
        
        if warn:
            warnings.warn("Converting models between 'cpu' and 'cuda' after "
                          "initializing optimizers can give errors when using "
                          "optimizers other than SGD or Adam!")
        
        self._enable_cuda = enable_cuda
        self.device = torch.device(
                'cuda' if torch.cuda.is_available() and self._enable_cuda \
                else 'cpu')
        self.pi.to(self.device)
        self.target_pi.to(self.device)
        self.Q.to(self.device)
        self.target_Q.to(self.device)
        
    def load_models(self, filename, enable_cuda=True, continue_training=True):
        """Load policy and value functions. Copy them to target functions."""
        
        models = torch.load(filename)

        self.pi.load_state_dict(models['pi_state_dict'])
        self.target_pi = copy.deepcopy(self.pi)
        self.Q.load_state_dict(models['Q_state_dict'])
        self.target_Q = copy.deepcopy(self.Q)
        
        if continue_training:
            self.pi.train()
            self.target_pi.train()
        else:
            self.pi.eval()
            self.target_pi.eval()
            
        self.enable_cuda(enable_cuda, warn=False)
        
    def save_checkpoint(self, filename):
        """Save state_dicts of models and optimizers."""
        
        torch.save({
                'using_cuda': self._enable_cuda,
                'pi_state_dict': self.pi.state_dict(),
                'target_pi_state_dict': self.target_pi.state_dict(),
                'Q_state_dict': self.Q.state_dict(),
                'target_Q_state_dict': self.target_Q.state_dict(),
                'pi_optimizer_state_dict': self.pi_optim.state_dict(),
                'Q_optimizer_state_dict': self.Q_optim.state_dict(),
        }, filename)
    
    def load_checkpoint(self, filename, continue_training=True):
        """Load state_dicts for models and optimizers."""
        
        checkpoint = torch.load(filename)
        
        self.pi.load_state_dict(checkpoint['pi_state_dict'])
        self.target_pi.load_state_dict(checkpoint['target_pi_state_dict'])
        self.Q.load_state_dict(checkpoint['Q_state_dict'])
        self.target_Q.load_state_dict(checkpoint['target_Q_state_dict'])
        self.pi_optim.load_state_dict(checkpoint['pi_optimizer_state_dict'])
        self.Q_optim.load_state_dict(checkpoint['Q_optimizer_state_dict'])
        
        if continue_training:
            self.pi.train()
            self.target_pi.train()
            self.Q.train()
            self.target_Q.train()
            
        else:
            self.pi.eval()
            self.target_pi.eval()
            self.Q.eval()
            self.target_Q.eval()
        
        self.enable_cuda(checkpoint['using_cuda'], warn=False)



        
        
# end
