import numpy as np
import torch

import rlcontrol.core.core as core
import rlcontrol.core.utils as utils
import rlcontrol.core.envs as envs


random_seed = 1994
models_filename = 'pretrain_models.pth'
buff_len = 100000
noise_covariance = 1e-5
num_hidden_units = 512
batch_size = 64
pi_lr = 0.001
Q_lr = 0.001
gamma = 0.99
tau = 0.001
enable_cuda = False
grad_clip_radius = None
episodes = 20
episode_length = 100
state_dim = 360
action_dim = 360
state_clip_radius = 1.0

np.random.seed(random_seed)

buffer = utils.Buffer(buff_len)

pi = torch.nn.Sequential(
    torch.nn.Linear(state_dim, num_hidden_units),
    torch.nn.ReLU(),
    torch.nn.Linear(num_hidden_units, num_hidden_units),
    torch.nn.ReLU(),
    torch.nn.Linear(num_hidden_units, action_dim),
)

Q = torch.nn.Sequential(
    torch.nn.Linear(state_dim + action_dim, num_hidden_units),
    torch.nn.ReLU(),
    torch.nn.Linear(num_hidden_units, num_hidden_units),
    torch.nn.ReLU(),
    torch.nn.Linear(num_hidden_units, 1),
)

noise = utils.GaussianNoise(action_dim, cov=noise_covariance * np.eye(action_dim))
agent = core.DDPGAgent(buffer, batch_size, noise, pi, Q, pi_lr, Q_lr,
                       gamma, tau, enable_cuda, grad_clip_radius=grad_clip_radius)
agent.load_models('pretrain_models.pth')

env = envs.NoiselessProjectEnv(state_clip_radius=state_clip_radius)

# add to buffer
for _ in range(5*batch_size):
    action = agent.sample_action(env.state)
    reward, next_state = env.transition(action)
    agent.update(reward, next_state, False, False)
    
for episode in range(episodes):
    episode_reward = 0.0
    for i in range(episode_length):
        action = agent.sample_action(env.state)
        reward, next_state = env.transition(action)
        agent.update(reward, next_state)
        episode_reward += reward
    print(f'Episode {episode} average reward: {episode_reward / episode_length}')
        











# end
