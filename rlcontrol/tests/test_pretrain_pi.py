# Test to check if pretrained policy works

import numpy as np
import torch
from time import sleep

import rlcontrol.core.envs as envs
import rlcontrol.core.core as core
import rlcontrol.core.utils as utils


num_steps = 100

config = utils.load_config('../scripts/config.yml')

matrix_filename = config['matrix_filename']
random_seed = config['random_seed']
buff_len = config['buff_len']
noise_covariance = config['noise_covariance']
pi_hidden_units = config['pi_hidden_units']
Q_hidden_units = config['Q_hidden_units']
train_batch_size = config['train_batch_size']
pi_lr = config['pi_lr']
Q_lr = config['Q_lr']
gamma = config['gamma']
tau = config['tau']
enable_cuda = config['enable_cuda']
grad_clip_radius = config['grad_clip_radius']
pretrain_parameters_filename = config['pretrain_parameters_filename']

matrix = np.load(matrix_filename)
action_dim, state_dim = matrix.shape    
del matrix

np.random.seed(random_seed)

buffer = utils.Buffer(buff_len)

pi = torch.nn.Sequential(
    torch.nn.Linear(state_dim, pi_hidden_units),
    torch.nn.ReLU(),
    torch.nn.Linear(pi_hidden_units, pi_hidden_units),
    torch.nn.ReLU(),
    torch.nn.Linear(pi_hidden_units, action_dim),
)

Q = torch.nn.Sequential(
    torch.nn.Linear(state_dim + action_dim, Q_hidden_units),
    torch.nn.ReLU(),
    torch.nn.Linear(Q_hidden_units, Q_hidden_units),
    torch.nn.ReLU(),
    torch.nn.Linear(Q_hidden_units, 1),
)

noise = utils.GaussianNoise(action_dim,
                            cov=noise_covariance * np.eye(action_dim))
agent = core.DDPGAgent(buffer, train_batch_size, noise, pi, Q, pi_lr, Q_lr,
                       gamma, tau, enable_cuda,
                       grad_clip_radius=grad_clip_radius)
agent.load_models(pretrain_parameters_filename)

env = envs.NoiselessProjectEnv(matrix_filename)
env.state = np.random.uniform(-1,1,state_dim)


for i in range(num_steps):
    sleep(1)
    action = agent.sample_action(env.state)
    reward, next_state = env.transition(action)
    print(reward)
#    print(next_state)
