# Test env to make sure using pseudoinverse works

import numpy as np
import torch
from time import sleep

import rlcontrol.core.envs as envs


num_steps = 100
matrix_filename = '../data/A_Matrix.npy'

matrix = np.load(matrix_filename)
state_dim = matrix.shape    
del matrix

env = envs.NoiselessProjectEnv(matrix_filename)
env.state = np.random.uniform(-1,1,state_dim)

R = np.load(matrix_filename)
Rpinv = np.linalg.pinv(R)

def get_action(state):
    return Rpinv.dot(state)

for i in range(num_steps):
    sleep(1)
    action = get_action(env.state)
    reward, next_state = env.transition(action)
    print(reward)
#    print(next_state)
