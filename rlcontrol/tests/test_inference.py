# Test to check if pretrained policy computes the pseudoinverse

import numpy as np
import torch

import rlcontrol.core.utils as utils


config = utils.load_config('../scripts/config.yml')

matrix_filename = config['matrix_filename']
pi_hidden_units = config['pi_hidden_units']
pretrain_parameters_filename = config['pretrain_parameters_filename']

R = np.load(matrix_filename)
action_dim, state_dim = R.shape    

policy = torch.nn.Sequential(
    torch.nn.Linear(state_dim, pi_hidden_units),
    torch.nn.ReLU(),
    torch.nn.Linear(pi_hidden_units, pi_hidden_units),
    torch.nn.ReLU(),
    torch.nn.Linear(pi_hidden_units, action_dim),
)

infer = utils.inference_function(policy, pretrain_parameters_filename)

for _ in range(100):
    state = np.random.uniform(-1,1,(state_dim,))
    action = infer(state)
    inv = R.dot(action)
    print('Error between state and R*action: {}'.format(
        np.linalg.norm(state - inv)))
