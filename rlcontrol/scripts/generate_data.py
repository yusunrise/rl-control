# Generate training data

import numpy as np

from rlcontrol.core.utils import DataGenerator
from rlcontrol.core.utils import load_config


if __name__ == '__main__':

    config = load_config('config.yml')

    matrix_filename = config['matrix_filename']
    num_pretrain_samples = config['num_pretrain_samples']
    pretrain_radius = config['pretrain_radius']
    pretrain_data_filename = config['pretrain_data_filename']
    num_pretest_samples = config['num_pretest_samples']
    pretest_radius = config['pretest_radius']
    pretest_data_filename = config['pretest_data_filename']
                
    R = np.load(matrix_filename)
    Rpseudoinv = np.linalg.pinv(R)
    print(R.shape)
    state_dim = Rpseudoinv.shape[0]

    # action_generator
    def f(x):
        return Rpseudoinv.dot(x)

    # state_generator
    def pretrain_state_generator():
        return np.random.uniform(
            -pretrain_radius, pretrain_radius, state_dim)

    def pretest_state_generator():
        return np.random.uniform(
            -pretest_radius, pretest_radius, state_dim)


    pretrain_gen = DataGenerator(
        pretrain_state_generator, f, num_pretrain_samples)
    pretrain_gen.generate_data()
    pretrain_gen.save_data(pretrain_data_filename)

    pretest_gen = DataGenerator(
        pretest_state_generator, f, num_pretest_samples)
    pretest_gen.generate_data()
    pretest_gen.save_data(pretest_data_filename)
