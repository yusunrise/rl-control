# Pretrain policy and Q function

import numpy as np
import torch

import rlcontrol.core.core as core
import rlcontrol.core.utils as utils
import rlcontrol.core.envs as envs


if __name__ == '__main__':

    config = utils.load_config('config.yml')

    matrix_filename = config['matrix_filename']
    pi_hidden_units = config['pi_hidden_units']
    Q_hidden_units = config['Q_hidden_units']
    pretrain_batch_size = config['pretrain_batch_size']
    gamma = config['gamma']
    enable_cuda = config['enable_cuda']
    pretrain_parameters_filename = config['pretrain_parameters_filename']
    num_epochs = config['num_epochs']
    pretrain_data_filename = config['pretrain_data_filename']
    pretest_data_filename = config['pretest_data_filename']
    errors_filename = config['errors_filename']
    log_interval = config['log_interval']
    pretrain_checkpoint_filename = config['pretrain_checkpoint_filename']
    pretrain_loading_checkpoint = config['pretrain_loading_checkpoint']
    train_pi = config['train_pi']

    matrix = np.load(matrix_filename)
    action_dim, state_dim = matrix.shape    
    del matrix

    # pre-pi
    pi = torch.nn.Sequential(
        torch.nn.Linear(state_dim, pi_hidden_units),
        torch.nn.ReLU(),
        torch.nn.Linear(pi_hidden_units, pi_hidden_units),
        torch.nn.ReLU(),
        torch.nn.Linear(pi_hidden_units, action_dim),
    )

    # pre-Q
    Q = torch.nn.Sequential(
        torch.nn.Linear(state_dim + action_dim, Q_hidden_units),
        torch.nn.ReLU(),
        torch.nn.Linear(Q_hidden_units, Q_hidden_units),
        torch.nn.ReLU(),
        torch.nn.Linear(Q_hidden_units, 1),
    )

    env = envs.NoiselessProjectEnv(matrix_filename)

    trainer = core.Trainer(pi, Q, env, pretrain_data_filename,
                           pretest_data_filename,
                           pretrain_parameters_filename, errors_filename,
                           num_epochs, pretrain_batch_size, gamma,
                           log_interval=log_interval,
                           enable_cuda=enable_cuda)

    if pretrain_loading_checkpoint:
        trainer.load_checkpoint(pretrain_checkpoint_filename)

    trainer.train(train_pi)

    trainer.save_checkpoint(pretrain_checkpoint_filename)
