# Train RL agent

import numpy as np
import torch

import rlcontrol.core.core as core
import rlcontrol.core.utils as utils
import rlcontrol.core.envs as envs


if __name__ == '__main__':

    config = utils.load_config('config.yml')

    matrix_filename = config['matrix_filename']
    random_seed = config['random_seed']
    buff_len = config['buff_len']
    noise_covariance = config['noise_covariance']
    pi_hidden_units = config['pi_hidden_units']
    Q_hidden_units = config['Q_hidden_units']
    train_batch_size = config['train_batch_size']
    pi_lr = config['pi_lr']
    Q_lr = config['Q_lr']
    gamma = config['gamma']
    tau = config['tau']
    enable_cuda = config['enable_cuda']
    grad_clip_radius = config['grad_clip_radius']
    episodes = config['episodes']
    episode_length = config['episode_length']
    state_clip_radius = config['state_clip_radius']
    pretrain_parameters_filename = config['pretrain_parameters_filename']
    train_checkpoint_filename = config['train_checkpoint_filename']
    train_loading_checkpoint = config['train_loading_checkpoint']
    rewards_filename = config['rewards_filename']

    matrix = np.load(matrix_filename)
    action_dim, state_dim = matrix.shape    
    del matrix

    np.random.seed(random_seed)

    buffer = utils.Buffer(buff_len)

    pi = torch.nn.Sequential(
        torch.nn.Linear(state_dim, pi_hidden_units),
        torch.nn.ReLU(),
        torch.nn.Linear(pi_hidden_units, pi_hidden_units),
        torch.nn.ReLU(),
        torch.nn.Linear(pi_hidden_units, action_dim),
    )

    Q = torch.nn.Sequential(
        torch.nn.Linear(state_dim + action_dim, Q_hidden_units),
        torch.nn.ReLU(),
        torch.nn.Linear(Q_hidden_units, Q_hidden_units),
        torch.nn.ReLU(),
        torch.nn.Linear(Q_hidden_units, 1),
    )

    noise = utils.GaussianNoise(action_dim,
                                cov=noise_covariance * np.eye(action_dim))
    agent = core.DDPGAgent(buffer, train_batch_size, noise, pi, Q, pi_lr, Q_lr,
                           gamma, tau, enable_cuda,
                           grad_clip_radius=grad_clip_radius)

    if train_loading_checkpoint:
        agent.load_checkpoint(train_checkpoint_filename)
    else:
        agent.load_models(pretrain_parameters_filename)

    env = envs.NoiselessProjectEnv(matrix_filename,
                                   state_clip_radius=state_clip_radius)

    # add to buffer
    for _ in range(2*train_batch_size):
        action = agent.sample_action(env.state)
        reward, next_state = env.transition(action)
        agent.update(reward, next_state, False, False)
        
    # train, finally
    episode_rewards = []
    for episode in range(episodes):
        episode_reward = 0.0
        for i in range(episode_length):
            action = agent.sample_action(env.state)
            reward, next_state = env.transition(action)
            agent.update(reward, next_state)
            episode_reward += reward
        episode_rewards.append(episode_reward)

        print(f'Episode {episode} average reward: {episode_reward / episode_length}')

        agent.save_checkpoint(train_checkpoint_filename)
        np.save(rewards_filename, episode_rewards)
            











# end
